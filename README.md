GryterGeneratorBundle
=====================

The `GryterGeneratorBundle` extends `SensioGeneratorBundle` and add new skeletons for CRUD controllers to generate them compatible with Bootstrap 3.x.

Installation
------------

Require the `gryter/generator-bundle` package in your composer.json and update
your dependencies.

    $ composer require gryter/generator-bundle

Enable both bundles by adding them to the list of registered bundles for the dev environment in the `app/AppKernel.php` file of your project:

```php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
	public function registerBundles()
	{
		if (in_array($this->getEnvironment(), array('dev', 'test'))) {
			$bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
			$bundles[] = new Gryter\GeneratorBundle\GryterGeneratorBundle();
			// ...
		}

		// ...
	}

	// ...
}
```

Usage
-----

Check `SensioGeneratorBundle ` official [documentation](http://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html).
