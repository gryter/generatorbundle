<?php

namespace Gryter\GeneratorBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\DoctrineCrudGenerator as BaseDoctrineCrudGenerator;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

class DoctrineCrudGenerator extends BaseDoctrineCrudGenerator
{
	/**
	 * {@inheritdoc}
	 */
	public function generate(BundleInterface $bundle, $entity, ClassMetadataInfo $metadata, $format, $routePrefix, $needWriteActions, $forceOverwrite)
	{
		parent::generate($bundle, $entity, $metadata, $format, $routePrefix, $needWriteActions, $forceOverwrite);

		$dir = sprintf('%s/../web/js', $this->rootDir);

		$this->generateAppJs($dir);
	}

	/**
	 * Generates the new.html.twig template in the final bundle.
	 *
	 * @param string $dir The path to the folder that hosts templates in the bundle
	 */
	protected function generateAppJs($dir)
	{
		$dest = $dir.'/confirmation.js';
		if( file_exists($dest) ) {
			return;
		}
		$this->renderFile('crud/javascripts/confirmation.js.twig', $dest, array());
	}
}
