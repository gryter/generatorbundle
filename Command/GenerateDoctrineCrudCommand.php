<?php

namespace Gryter\GeneratorBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\GenerateDoctrineCrudCommand as BaseGenerateDoctrineCrudCommand;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Gryter\GeneratorBundle\Generator\DoctrineCrudGenerator;

/**
 * Generates a CRUD for a Doctrine entity.
 */
class GenerateDoctrineCrudCommand extends BaseGenerateDoctrineCrudCommand
{
	protected function getSkeletonDirs(BundleInterface $bundle = null)
	{
		$dirs = parent::getSkeletonDirs($bundle);

		$count = count($dirs);
		$start = array_slice( $dirs, 0, $count-2 );
		$end = array_slice( $dirs, $count-2 );

		$new_dirs = array(
			__DIR__.'/../Resources/skeleton',
			__DIR__.'/../Resources',
		);

		return array_merge( $start, $new_dirs, $end );
	}

	protected function createGenerator($bundle = null)
	{
		return new DoctrineCrudGenerator(
			$this->getContainer()->get('filesystem'),
			$this->getContainer()->getParameter('kernel.root_dir')
		);
	}
}
