<?php

namespace Gryter\GeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GryterGeneratorBundle extends Bundle
{
	public function getParent()
	{
		return 'SensioGeneratorBundle';
	}
}
